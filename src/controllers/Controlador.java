/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import models.Alumno;
import models.ModeloArrayList;
import models.ModeloDb4o;
import views.VistaPantalla;

/**
 *
 * @author juanma navarro
 * @email juanaco1.u@gmail.com
 * @fecha 25/2/2015
 */
public class Controlador implements ActionListener {
    
    protected Alumno alumno;
    protected ModeloDb4o listaAlumnos;
    protected VistaPantalla vistaP;

    Controlador(VistaPantalla vp, ModeloDb4o mal) {
        vistaP = vp;
        listaAlumnos = mal;
        
        vistaP.getBtnCreate().addActionListener(this);
        vistaP.getBtnRead().addActionListener(this);
        vistaP.getBtnUpdate().addActionListener(this);
        vistaP.getBtnDelete().addActionListener(this);
        vistaP.getBtnExit().addActionListener(this);
    }

    public void crearAlumno(String id, String nombre, int edad, String email) {
        Alumno al = new Alumno(id, nombre, edad, email);
        listaAlumnos.create(al);
    }
    
    public Alumno leerAlumno(int id) {
        //return listaAlumnos.readAlumno(id);
        return listaAlumnos.readAlumno(id);
    }
    
    public void borrarAlumno(int id) {
        listaAlumnos.delete(id);
    }
    
    public void editarAlumno(int id, Alumno alumno) {
        listaAlumnos.update(id, alumno);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int idd;
        Alumno al;
        JTextField idField, nombreField, edadField, emailField;
        String id, nombre, edad, email;
        
        idField = vistaP.getIdField();
        nombreField = vistaP.getNombreField();
        edadField = vistaP.getEdadField();
        emailField = vistaP.getEmailField();
        
        id = idField.getText();
        nombre = nombreField.getText();
        edad = edadField.getText();
        email = emailField.getText();
        
        String command = e.getActionCommand();
        switch(command) {
            case "create":
                if(id.equals("") || nombre.equals("") || edad.equals("") || email.equals("")) {
                    vistaP.crearAviso("Todos los campos son obligatorios");
                    return;
                }
                int edadInt = Integer.parseInt(edad);
                al = leerAlumno(Integer.parseInt(id));
                if(al!=null) {
                    vistaP.crearAviso("El id de alumno ya existe");
                    return;
                }
                crearAlumno(id, nombre, edadInt, email);
                restartFields(idField, nombreField, edadField, emailField);
                break;
            case "read":
                idd = Integer.parseInt(vistaP.idDialog());
                al = leerAlumno(idd);
                if(al==null) {
                    vistaP.crearAviso("El alumno no existe");
                    return;
                }
                vistaP.getNombreOField().setText(al.getNombre());
                vistaP.getEdadOField().setText(Integer.toString(al.getEdad()));
                vistaP.getEmailOField().setText(al.getEmail());
                break;
            case "update":
                if(id.equals("") || nombre.equals("") || edad.equals("") || email.equals("")) {
                    idd = Integer.parseInt(vistaP.idDialog());
                    al = leerAlumno(idd);
                    if(al==null) {
                        vistaP.crearAviso("El alumno no existe");
                        return;
                    }
                    vistaP.getIdField().setText(al.getId());
                    vistaP.getNombreField().setText(al.getNombre());
                    vistaP.getEdadField().setText(Integer.toString(al.getEdad()));
                    vistaP.getEmailField().setText(al.getEmail());
                    
                    vistaP.getIdField().setEditable(false);
                } else {
                    int i = Integer.parseInt(id);
                    
                    edadInt = Integer.parseInt(edad);
                    
                    al = new Alumno(id, nombre, edadInt, email);
                   
                    editarAlumno(i, al);
                    restartFields(idField, nombreField, edadField, emailField);
                    vistaP.getIdField().setEditable(true);
                }
                break;
            case "delete":
                idd = Integer.parseInt(vistaP.idDialog());
                al = leerAlumno(idd);
                if(al==null) {
                    vistaP.crearAviso("El alumno no existe");
                    return;
                } else {
                    borrarAlumno(idd);
                    vistaP.crearAviso("El alumno ha sido borrado");
                }
                break;
            case "exit":
                vistaP.dispose();
                break;
        }
    
    }

    private void restartFields(JTextField idField, JTextField nombreField, JTextField edadField, JTextField emailField) {
        idField.setText("");
        nombreField.setText("");
        edadField.setText("");
        emailField.setText("");
    }
    
}
