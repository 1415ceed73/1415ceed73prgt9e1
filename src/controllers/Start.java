/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.sql.SQLException;
import models.ModeloArrayList;
import models.ModeloDb4o;
import views.VistaPantalla;

/**
 *
 * @author juanma navarro
 * @email juanaco1.u@gmail.com
 * @fecha 25/2/2015
 */
public class Start {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        VistaPantalla vp = new VistaPantalla();
        ModeloDb4o mal = new ModeloDb4o();
        
        Controlador c = new Controlador(vp, mal);
    }
}
