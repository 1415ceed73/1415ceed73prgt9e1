/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author juanma navarro
 * @email juanaco1.u@gmail.com
 * @fecha 25/2/2015
 */
public interface Modelo {
    
    public void create(Alumno alumno); // Crea un alumno nuevo
    public ArrayList read(); // Muestra los alunnos.
    public void update(int id, Alumno alumno); // Actuzaliza el alumno.
    public void delete(int id); // Borrar el alunno con el id dado
    public int getId(); // Obtiene el último id dado. Usado para create.
    
}
