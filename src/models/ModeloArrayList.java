/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author juanma navarro
 * @email juanaco1.u@gmail.com
 * @fecha 25/2/2015
 */
public class ModeloArrayList implements Modelo {
    
    protected ArrayList al = new ArrayList<>();

    @Override
    public void create(Alumno alumno) {
        al.add(alumno);
    }

    @Override
    public ArrayList read() {
        return this.al;
    }

    @Override
    public void update(int id, Alumno alumno) {
        Alumno alu = this.readAlumno(id);
        this.delete(id);
        alu.setNombre(alumno.getNombre());
        alu.setEdad(alumno.getEdad());
        alu.setEmail(alumno.getEmail());
        this.al.add(alu);
    }

    @Override
    public void delete(int id) {
        Alumno al = this.readAlumno(id);
        this.al.remove(al);
    }

    @Override
    public int getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Alumno readAlumno(int id) {
        Iterator it = al.iterator();
        while(it.hasNext()) {
            Alumno alu = (Alumno)it.next();
            if(Integer.parseInt(alu.getId()) == id) {
                return alu;
            }
        }
        
        return null;
    }
    
}
