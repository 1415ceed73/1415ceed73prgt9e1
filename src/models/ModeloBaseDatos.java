/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mysql.jdbc.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author juanma
 */
public class ModeloBaseDatos implements Modelo {
    
    Connection con;
    Statement st;
    PreparedStatement preparedStatement;
    ResultSet resultSet;
    
    public ModeloBaseDatos() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/ceedprgt8";
        String login = "root";
        String password = "0006";
        
        con = (java.sql.DriverManager.getConnection(url, login, password));
    }

    @Override
    public void create(Alumno alumno) {

        try {
            st = con.createStatement();
            preparedStatement = con
                .prepareStatement("insert into  alumnos values (default, ?, ?, ?)");
            preparedStatement.setString(1, alumno.getNombre());
            preparedStatement.setInt(2, alumno.getEdad());
            preparedStatement.setString(3, alumno.getEmail());
            preparedStatement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(ModeloBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    public Alumno readAlumno(int id) {
        
        String nombre, email;
        int edad;
        
        try {
            st = con.createStatement();
            preparedStatement = con
                    .prepareStatement("select * from alumnos where id = ? limit 1");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            
            if(resultSet.first()) {
                nombre = resultSet.getString("nombre");
                edad = resultSet.getInt("edad");
                email = resultSet.getString("email");

                return new Alumno(Integer.toString(id), nombre, edad, email);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ModeloBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ArrayList read() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(int id, Alumno alumno) {
        
        try {
            st = con.createStatement();
            preparedStatement = con
                    .prepareStatement("update alumnos set nombre=?, edad=?, email=? where id=?");
            preparedStatement.setInt(4, id);
            preparedStatement.setString(1, alumno.getNombre());
            preparedStatement.setInt(2, alumno.getEdad());
            preparedStatement.setString(3, alumno.getEmail());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ModeloBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }

        //return null;

        //return null;
    }

    @Override
    public void delete(int id) {

        try {
            st = con.createStatement();
            preparedStatement = con
                .prepareStatement("delete from alumnos where id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(ModeloBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
