/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.db4o.*;
import com.db4o.query.Predicate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author juanma
 */
public class ModeloDb4o implements Modelo {
    
    ObjectContainer bd;
    
//    public ModeloDb4o() throws ClassNotFoundException, SQLException {
//        bd = Db4oEmbedded.openFile("databaseFile.db4o");
//    }

    @Override
    public void create(Alumno alumno) {

        try {
            bd = Db4oEmbedded.openFile("databaseFile.db4o");
            bd.store(alumno);
        } catch (Exception ex) {
            Logger.getLogger(ModeloDb4o.class.getName()).log(Level.SEVERE, null, ex);
            
        }  finally {
            bd.close();
        }
    }
    
    public Alumno readAlumno(int id) {
        
        try {
            bd = Db4oEmbedded.openFile("databaseFile.db4o");
            ObjectSet listaAl = bd.query(new Predicate<Alumno>() {
                @Override
                public boolean match(Alumno al) {
                    return al.getId().equals(Integer.toString(id));
                }
            });
            
            Alumno result = null;
            while(listaAl.hasNext()) {
                 result = (Alumno) listaAl.next();
            }
            
            return result;
        } catch(Exception e) {
            
        } finally {
            bd.close();
        }
        
        return null;
    }

    @Override
    public ArrayList read() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(int id, Alumno alumno) {
        try {
            bd = Db4oEmbedded.openFile("databaseFile.db4o");
            ObjectSet listaAl = bd.query(new Predicate<Alumno>() {
                @Override
                public boolean match(Alumno al) {
                    return al.getId().equals(Integer.toString(id));
                }
            });
            
            Alumno al2 = null;
            while(listaAl.hasNext()) {
                 al2 = (Alumno) listaAl.next();
            }
            
//            Alumno al = new Alumno(Integer.toString(id), null, 0, null);
//            ObjectSet res = bd.queryByExample(al);
//            Alumno al2 = (Alumno)res.next();
            al2.setNombre(alumno.getNombre());
            al2.setEdad(alumno.getEdad());
            al2.setEmail(alumno.getEmail());
            bd.store(al2);
        } catch (Exception e) {
            
        } finally {
            bd.close();
        }
    }

    @Override
    public void delete(int id) {
        try {
             bd = Db4oEmbedded.openFile("databaseFile.db4o");
            ObjectSet listaAl = bd.query(new Predicate<Alumno>() {
                @Override
                public boolean match(Alumno al) {
                    return al.getId().equals(Integer.toString(id));
                }
            });
            
            Alumno al2 = null;
            while(listaAl.hasNext()) {
                 al2 = (Alumno) listaAl.next();
            }
            bd.delete(al2);
        } catch (Exception e) {
            
        } finally {
            bd.close();
        }
    }

    @Override
    public int getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
