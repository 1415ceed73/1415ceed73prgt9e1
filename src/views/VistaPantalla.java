/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author juanma navarro
 * @email juanaco1.u@gmail.com
 * @fecha 25/2/2015
 */
public class VistaPantalla extends JFrame {
    
    private JButton btnExit, btnCreate, btnRead, btnUpdate, btnDelete;
    private JTextField idField, nombreField, edadField, emailField;
    private JTextField nombreOField, edadOField, emailOField;
    
    public VistaPantalla() {
        super("Alumno");
        
        this.configurarVentana();
        this.crearInterfaz(); 
    }

    public JButton getBtnExit() {
        return btnExit;
    }

    public JButton getBtnCreate() {
        return btnCreate;
    }

    public JButton getBtnRead() {
        return btnRead;
    }

    public JButton getBtnUpdate() {
        return btnUpdate;
    }

    public JButton getBtnDelete() {
        return btnDelete;
    }

    public JTextField getIdField() {
        return idField;
    }

    public JTextField getNombreField() {
        return nombreField;
    }

    public JTextField getEdadField() {
        return edadField;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JTextField getNombreOField() {
        return nombreOField;
    }

    public JTextField getEdadOField() {
        return edadOField;
    }

    public JTextField getEmailOField() {
        return emailOField;
    }
    
    
    private void configurarVentana() {
        this.setSize(800, 300);
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    private void crearInterfaz() {
        
        GridLayout mainLayout = new GridLayout();
        mainLayout.setColumns(1);
        mainLayout.setRows(2);
        this.setLayout(mainLayout);
        
        JPanel upRow = new JPanel();
        GridLayout upLayout = new GridLayout();
        upLayout.setColumns(2);
        upLayout.setRows(1);
        upRow.setLayout(upLayout);
        
        upRow.add(composeInputPanel());
        upRow.add(composeOutputPanel());
        
        this.add(upRow);
        this.add(composeButtonPanel());
        
        this.setVisible(true);
    }
    
    private JComponent composeButtonPanel() {
        
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        
        btnCreate = new JButton("Crear");
        btnCreate.setActionCommand("create");
        
        btnRead = new JButton("Leer");
        btnRead.setActionCommand("read");
        
        btnUpdate = new JButton("Editar");
        btnUpdate.setActionCommand("update");
        
        btnDelete = new JButton("Borrar");
        btnDelete.setActionCommand("delete");
        
        btnExit = new JButton("Salir");
        btnExit.setActionCommand("exit");
        
        panel.add(btnCreate);
        panel.add(btnRead);
        panel.add(btnUpdate);
        panel.add(btnDelete);
        panel.add(btnExit);
        
        return panel;
    }

    private JComponent composeInputPanel() {
        JLabel idLabel = new JLabel();
        idLabel.setText("Id");
        idField = new JTextField();
        idField.setColumns(20);
        
        JLabel nombreLabel = new JLabel();
        nombreLabel.setText("Nombre");
        nombreField = new JTextField();
        nombreField.setColumns(20);
        
        JLabel edadLabel = new JLabel();
        edadLabel.setText("Edad");
        edadField = new JTextField();
        edadField.setColumns(20);
        
        JLabel emailLabel = new JLabel();
        emailLabel.setText("Email");
        emailField = new JTextField();
        emailField.setColumns(20);
        
        JPanel inputPanel = new JPanel();
        GridLayout inputLayout = new GridLayout();
        inputLayout.setColumns(1);
        inputLayout.setRows(4);
        inputPanel.setLayout(inputLayout);
        inputPanel.add(setFormFieldLayout(idLabel, idField));
        inputPanel.add(setFormFieldLayout(nombreLabel, nombreField));
        inputPanel.add(setFormFieldLayout(edadLabel, edadField));
        inputPanel.add(setFormFieldLayout(emailLabel, emailField));
        
        return inputPanel;
    }
    
    private JComponent composeOutputPanel() {
        
        JLabel nombreLabel = new JLabel();
        nombreLabel.setText("Nombre");
        nombreOField = new JTextField();
        nombreOField.setColumns(20);
        nombreOField.setEditable(false);
        
        JLabel edadLabel = new JLabel();
        edadLabel.setText("Edad");
        edadOField = new JTextField();
        edadOField.setColumns(20);
        edadOField.setEditable(false);
        
        JLabel emailLabel = new JLabel();
        emailLabel.setText("Email");
        emailOField = new JTextField();
        emailOField.setColumns(20);
        emailOField.setEditable(false);
        
        JPanel inputPanel = new JPanel();
        GridLayout inputLayout = new GridLayout();
        inputLayout.setColumns(1);
        inputLayout.setRows(4);
        inputPanel.setLayout(inputLayout);
        inputPanel.add(setFormFieldLayout(nombreLabel, nombreOField));
        inputPanel.add(setFormFieldLayout(edadLabel, edadOField));
        inputPanel.add(setFormFieldLayout(emailLabel, emailOField));
        inputPanel.add(new JLabel());
        
        return inputPanel;
    }

    private JComponent setFormFieldLayout(JLabel label, JTextField field) {
        JPanel formField = new JPanel();
        GridLayout gr = new GridLayout();
        gr.setColumns(2);
        gr.setRows(1);
        formField.setLayout(gr);
        formField.add(label);
        formField.add(field);
        
        return formField;
    }
    
    public String idDialog() {
        String id = (String)JOptionPane.showInputDialog(
            this,
            "Id?",
            "Consulta",
            JOptionPane.PLAIN_MESSAGE
        );
        
        return id;

    } 
    
    public void crearAviso(String mensaje) {
        JOptionPane.showMessageDialog(this,
            mensaje,
            "Atención",
            JOptionPane.ERROR_MESSAGE
        );
    }
    
}
